FROM alpine

WORKDIR /app

COPY site.lisp /src/site.lisp

EXPOSE 6453

RUN apk add sbcl

CMD ["sbcl", "--script", "/src/site.lisp"]
