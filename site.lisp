;;(load "~/.local/share/quicklisp/setup.lisp")
(require :sb-bsd-sockets)

(defun response-create (status text type)
  (list :status status :text text :type type))

(defparameter +address+ "0.0.0.0")
(defparameter +port+ 6453)
(defparameter +linebreak+ (concatenate 'string '(#\return) '(#\Newline)))
(defparameter +headers+ (format nil "Server: SBCL HTTP Server ~A
Connection: keep-alive ~A
~A
" #\return #\return #\return #\return))
(defparameter +404-response+ (response-create 404 "404" "text/html"))

(defun split-string (string seperator)
  (let ((split-pos (position seperator string)))
    (when split-pos
      (list (subseq string 0 split-pos)
            (subseq string (1+ split-pos))))))

(defun split-string-list (string seperator)
  (if string
      (let ((splitted (split-string string seperator)))
        (if splitted
            (let ((x (car splitted))
                  (xs (split-string-list (car (cdr splitted)) seperator)))
              (if xs
                  (concatenate 'list (list x) xs)
                  (list x)))
            (list string)))
      nil))

(defun split-by-lines (string)
  (split-string-list string #\Newline))

(defun path-protect (path)
  (if (and path (probe-file path))
      (if (search (namestring (truename (pathname "./"))) (namestring (truename (pathname path))))
          path
          nil)
      nil))

(defun path-override (path)
  (cond
    ((equal path "") "./index.html")
    ((equal path "favicon.ico") nil)
    (t path)))

(defun get-file-type (path)
  (let ((file-ext (pathname-type path)))
    (cond
      ((equal file-ext "html") "text/html")
      ((equal file-ext "css") "text/css"))))

(defun handle-request (path)
  (let ((realpath (path-protect (path-override path))))
    (if realpath
        (if (and (probe-file realpath) (pathname-name (probe-file realpath)))
            (with-open-file (file realpath :if-does-not-exist nil)
              (if file
                  (response-create 200
                                   (format nil "~{~A~^~%~}"
                                           (loop for line = (read-line file nil nil)
                                                 while line
                                                 collect line))
                                   (or (get-file-type realpath) "text/html"))
                  +404-response+))
            +404-response+)
        +404-response+)))

(defun get-request-path (request)
  (let ((req-top (or
                  (car (split-string request #\Newline))
                  request)))
    (subseq (nth 1 (split-string-list req-top #\space)) 1)))

(defun parse-request (request)
  (list
   :file-path (get-request-path request)))

(defun handle-connection (client-socket)
  (unwind-protect
       (progn
         (multiple-value-bind (buffer buffer-length address)
             (sb-bsd-sockets:socket-receive client-socket nil 1024)
           (let* ((raw-request (subseq buffer 0 buffer-length))
                  (request (parse-request raw-request))
                  (response (handle-request (getf request :file-path))))
             (sb-bsd-sockets:socket-send client-socket
                                         (format nil "HTTP/1.1 ~A ~A"
                                                 (getf response :status)
                                                 +linebreak+)
                                         nil)
             (sb-bsd-sockets:socket-send client-socket
                                         (format nil "Content-Type: ~A; charset=utf-8 ~A"
                                                 (getf response :type)
                                                 +linebreak+)
                                         nil)
             (sb-bsd-sockets:socket-send client-socket +headers+ nil)
             (sb-bsd-sockets:socket-send client-socket (getf response :text) nil))))
    (sb-bsd-sockets:socket-close client-socket)))

(let ((socket (make-instance 'sb-bsd-sockets:inet-socket
                                :protocol :tcp
                                :type :stream)))
     (unwind-protect
          (progn
            (setf (sb-bsd-sockets:sockopt-reuse-address socket) t)
            (sb-bsd-sockets:socket-bind
             socket
             (sb-bsd-sockets:make-inet-address +address+) +port+)
            (sb-bsd-sockets:socket-listen socket 3)
            (loop do (multiple-value-bind (client-socket client-peer)
                         (sb-bsd-sockets:socket-accept socket)
                       (sb-thread:make-thread
                        (lambda ()
                          (handle-connection client-socket))))))
       (sb-bsd-sockets:socket-close socket)))

;; (when nil (sb-thread:make-thread
;;  (lambda ()
;;    (let ((socket (make-instance 'sb-bsd-sockets:inet-socket
;;                                 :protocol :tcp
;;                                 :type :stream)))
;;      (unwind-protect
;;           (progn
;;             (setf (sb-bsd-sockets:sockopt-reuse-address socket) t)
;;             (sb-bsd-sockets:socket-bind
;;              socket
;;              (sb-bsd-sockets:make-inet-address +address+) +port+)
;;             (sb-bsd-sockets:socket-listen socket 3)
;;             (loop do (multiple-value-bind (client-socket client-peer)
;;                          (sb-bsd-sockets:socket-accept socket)
;;                        (sb-thread:make-thread
;;                         (lambda ()
;;                           (handle-connection client-socket))))))
;;        (sb-bsd-sockets:socket-close socket))))))

;; (when nil
;;   (let ((socket (make-instance 'sb-bsd-sockets:inet-socket
;;                                :protocol :tcp
;;                                :type :stream)))
;;     (unwind-protect
;;          (progn
;;            (setf (sb-bsd-sockets:sockopt-reuse-address socket) t)
;;            (sb-bsd-sockets:socket-bind
;;             socket
;;             (sb-bsd-sockets:make-inet-address +address+) +port+)
;;            (sb-bsd-sockets:socket-listen socket 3)
;;            (loop do (multiple-value-bind (client-socket client-peer)
;;                         (sb-bsd-sockets:socket-accept socket)
;;                       (handle-connection client-socket))))
;;       (sb-bsd-sockets:socket-close socket))))
